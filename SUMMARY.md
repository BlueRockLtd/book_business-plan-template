# Summary

* [Introduction](README.md)
* [Executive Summary](executive_summary)
* [Company Description](company_description.md)
* [Market Analysis](market_analysis.md)
* [Marketing/Sales Strategy](marketingsales_strategy.md)
* [Research & Development](research_&_development.md)
* [Staffing and Operations](staffing_and_operations.md)
* [Financial Projections](financial_projections.md)
* [Sales Pipeline](sales_pipeline.md)
* [Funding Requirements](funding_requirements.md)
* [Appendices](appendices.md)

